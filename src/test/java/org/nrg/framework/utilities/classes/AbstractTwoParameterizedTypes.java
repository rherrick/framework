package org.nrg.framework.utilities.classes;

public abstract class AbstractTwoParameterizedTypes<V> implements TwoParameterizedTypes<String, V> {
    public abstract String getKey();
}
