package org.nrg.framework.services;

public interface NoSuchService {
    String getName();
}
